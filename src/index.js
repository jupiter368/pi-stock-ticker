import {FetchStream} from 'fetch';
import CliTable from 'cli-table';

const SYMBOLS = [
  'GOOG',
  'FB',
  'VTI',
  'VXUS',
  'CHK',
  'ATW',
  'CVX',
  'BIIB',
  'ALXN',
  'TSLA',
  'SITO',
  'CMG',
  'CRM',
  'BABA',
  'AMZN',
  'NTNX',
  'HSON',
  'BBY',
  'ZTO',
  'SORL'
].sort();
const GOOGLE_FINANCE_URL = 'http://finance.google.com/finance/info';



const callback = function() {
  const fetch = new FetchStream(`${GOOGLE_FINANCE_URL}?q=${SYMBOLS.join(',')}`);
  fetch.on('data', chunk => {
    const table = new CliTable({
      head: ['Symbol', 'Price', 'l_fix', 'l_cur', 'Change', 'Change (%)', 'Volume', 'Last Trade Time', 'Index', 'Prevous Close Price'],
      style: {head: ['cyan']}
    });
    const tickers = chunk.toString('utf8', 4);
    const data = JSON.parse(tickers);
    data.forEach(t => {
      const row = [];
      row.push(t.t);
      row.push(t.l);
      row.push(t.l_fix);
      row.push(t.l_cur);
      row.push(t.c);
      row.push(t.cp);
      row.push(t.s);
      row.push(t.lt);
      row.push(t.e);
      row.push(t.pcls_fix);
      table.push(row);
    });
    console.log('\x1Bc');
    console.log(table.toString());
    console.log(new Date());
  });
}
callback();
setInterval(callback, 30000);
